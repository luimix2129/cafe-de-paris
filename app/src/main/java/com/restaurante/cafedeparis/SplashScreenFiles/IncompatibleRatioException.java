package com.restaurante.cafedeparis.SplashScreenFiles;

/**
 * Created by LuisHidalgo on 1/9/17.
 */

public class IncompatibleRatioException extends RuntimeException {

    private static final long serialVersionUID = 234608108593115395L;

    public IncompatibleRatioException() {
        super("Can't perform Ken Burns effect on rects with distinct aspect ratios!");
    }
}
